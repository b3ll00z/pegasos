import numpy as np
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.metrics import accuracy_score
from sklearn.metrics.pairwise import rbf_kernel


class Pegasos(BaseEstimator, ClassifierMixin):
	"""
	Implementation of Pegasos algorithm for an SVM binary classifier.
	This algorithm performs stochastic gradient descent, so that picking just
	one element at time, we're minimizing our objective function.
	As traditional SVM, the loss function used is hinge loss.
	Output classes are labeled by +1 and -1.
	"""

	def __init__(self, lambda_reg=0.01, n_iter=10):
		self.lambda_reg = lambda_reg
		self.n_iter = n_iter

	def sign(self, label, type_of_label):
		if label == type_of_label:
			return 1.
		return -1.

	def _find_classes(self, Y):
		"""Find the set of output classes in a training set.

		The attributes positive_class and negative_class will be assigned
		after calling this method. An exception is raised if the number of
		classes in the training set is not equal to 2."""
		classes = sorted(set(Y))

		if len(classes) != 2:
			raise Exception("This classifier works only on binary classification!")

		# 0 means not occupied, 1 - occupied
		self.positive_class_ = classes[1]
		self.negative_class_ = classes[0]

	def fit(self, X, Y):
		"""
		This is the heart of Pegasos. Here we're training our model so that
		we obtain the largest margin for those data-points at lowest distance
		from the hyperplane separator.n_iter
		"""
		self._find_classes(Y)

		# convert all output values to +1 or -1
		Yn = [self.sign(y, self.positive_class_) for y in Y]

		n_features = X.shape[1]
		self.w = np.zeros(n_features)

		for i in range(1, self.n_iter + 1):
			# Picking an example chosen randomly from our training set
			chosen_example = np.random.choice(X.shape[0], 1)[0]
			x, y = X[chosen_example], Yn[chosen_example]

			# Set step size for stochastic gradient descent, based on
			# regularization parameter and current iteration step
			learning_rate = 1 / (self.lambda_reg * i)

			# Calculate score
			score = self.w.dot(x)

			# Adjusting hyperplane in case of error
			if y * score < 1:
				self.w = (1 - learning_rate * self.lambda_reg) * self.w + learning_rate * y * x
			else:
				self.w = (1. - learning_rate * self.lambda_reg) * self.w

		return self

	def predict(self, X):
		scores = X.dot(self.w)
		predicted_labels = np.select([scores >= 0.0, scores < 0.0], [self.positive_class_, self.negative_class_])
		return predicted_labels

	def score(self, X, y, **kwargs):
		return accuracy_score(y, self.predict(X))


class Kernelized_Pegasos(Pegasos):

	def __init__(self, lambda_reg=0.01, n_iter=10, gamma=None):
		super().__init__(lambda_reg, n_iter)
		self.X_train_ = []
		self.gamma_ = gamma

	def fit(self, X_train, y_train):

		self._find_classes(y_train)

		# setting the kernel matrix
		K = rbf_kernel(X=X_train, gamma=self.gamma_)

		# convert all output values to +1 or -1
		Yn = [self.sign(y, self.positive_class_) for y in y_train]

		# Store training set for classification of un seen data, where is necessary
		# to create a similarity matrix between unseen data and training set used
		# for model construction
		self.X_train_ = X_train

		n_examples = X_train.shape[0]
		self.alphas = np.zeros(n_examples)

		for i in range(1, self.n_iter + 1):

			# Picking an example chosen randomly from our training set
			chosen_example = np.random.choice(X_train.shape[0], 1)[0]
			y = Yn[chosen_example]

			learning_rate = 1 / (self.lambda_reg * i)

			kernel_evaluations = [self.alphas[j] * y * K[chosen_example, j] for j in range(X_train.shape[0])
								  if j != chosen_example]

			score = y * learning_rate * np.sum(kernel_evaluations)

			if score < 1:
				self.alphas[chosen_example] += 1

		return self

	def predict(self, X_test):
		K = rbf_kernel(X=X_test, Y=self.X_train_, gamma=self.gamma_)
		kernel_evaluations = [[self.alphas[i] * K[j, i] for i in range(K.shape[1])] for j in range(K.shape[0])]

		scores = np.array([np.sum(k) for k in kernel_evaluations])
		predicted_labels = np.select([scores >= 0.0, scores < 0.0], [self.positive_class_, self.negative_class_])
		return predicted_labels
