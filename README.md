# Pegasos

My own implementation of Pegasos algorithm, an SVM classifier based on Stochastic Gradient Descent approach.

## Acknowledgment

### Pegasos algorithm
Shalev-Shwartz, S., Singer, Y., Srebro, N. et al. Math. Program. (2011) 127: 3. https://doi.org/10.1007/s10107-010-0420-4

A PDF version available here: https://ttic.uchicago.edu/~nati/Publications/PegasosMPB.pdf

### Dataset used
Accurate occupancy detection of an office room from light, temperature, humidity and CO2 measurements using statistical 
learning models. Luis M. Candanedo, Véronique Feldheime. Energy and Buildings. Volume 112, 15 January 2016, Pages 28-39.