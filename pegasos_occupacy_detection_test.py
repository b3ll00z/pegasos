import time
import matplotlib.pyplot as plt

from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier, export_graphviz

from other.grid_searchSVC import score
from pegasos import Pegasos
from utils import load_dataset, check_dataset_presence, plot_confusion_matrix

if __name__ == '__main__':

	# Dataset acquisition
	check_dataset_presence()
	data_training = load_dataset("dataset/datatraining.txt")
	data_test1 = load_dataset("dataset/datatest.txt")
	data_test2 = load_dataset("dataset/datatest2.txt")

	# Extracting training set and the two test sets
	X_train = data_training[data_training.columns.difference(["Occupancy"])].values
	y_train = data_training["Occupancy"].values

	X_test1 = data_test1[data_test1.columns.difference(["Occupancy"])].values
	y_test1 = data_test1["Occupancy"].values

	X_test2 = data_test2[data_test2.columns.difference(["Occupancy"])].values
	y_test2 = data_test2["Occupancy"].values

	# Preparing training and tests data for tree predictor later.
	# In fact, this kind of classifier works also without standardization
	X_train_tree, X_test1_tree, X_test2_tree = X_train, X_test1, X_test2

	# Standardization of feature vector only if do not want use bias term
	ss = StandardScaler()
	X_train = ss.fit_transform(X_train)
	X_test1 = ss.transform(X_test1)
	X_test2 = ss.transform(X_test2)

	# Preparing parameters for Grid Search
	# Note that this step may take long time. Uncomment for a better search
	# modifying tuning parameters.
	# tuning_parameters = {'lambda_reg':[7.74263683e-05, 5.99484250e-05, 4.64158883e-05, 3.59381366e-05, 2.78255940e-05,
	# 									 2.15443469e-05, 1.66810054e-05, 1.29154967e-05, 1.00000000e-05],
	# 					  'n_iter': [81430]}
	#
	# clf = GridSearchCV(Pegasos(), tuning_parameters, cv=10, scoring="%s_macro" % score)
	# Comment these three lines below in case you've uncommented GridSearch above
	clf = Pegasos(lambda_reg=2.15443469e-05, n_iter=81430)  # best params found by GridSearch

	print("Let's train our Pegasos model!")

	t0 = time.time()
	clf.fit(X_train, y_train)
	t1 = time.time()
	print('Training Pegasos time:', t1 - t0, 'seconds.')

	# Uncomment if you want to apply GridSearch for a better investigation of Pegasos' hyperparameters
	# print("Best parameters set found on development set:")
	# print(clf.best_params_)
	# print()

	# Pegasos score on unseen data
	print("Performance of my Pegasos own implementation")
	y_pegasos_train_guesses = clf.predict(X_train)
	y_pegasos_guesses1 = clf.predict(X_test1)
	y_pegasos_guesses2 = clf.predict(X_test2)

	print("Accuracy score for training set: {}".format(accuracy_score(y_train, y_pegasos_train_guesses)))
	print("Accuracy score for test_set_1: {}".format(accuracy_score(y_test1, y_pegasos_guesses1)))
	print("Accuracy score for test_set_2: {}".format(accuracy_score(y_test2, y_pegasos_guesses2)))

	print("Let's put same dataset on a tree predictor")

	tree = DecisionTreeClassifier(max_depth=4)
	tree.fit(X_train_tree, y_train)

	y_tree_train_guesses = tree.predict(X_train_tree)
	y_tree_guesses1 = tree.predict(X_test1_tree)
	y_tree_guesses2 = tree.predict(X_test2_tree)

	print("Accuracy score for training set: {}".format(accuracy_score(y_train, y_tree_train_guesses)))
	print("Accuracy score for test_set_1: {}".format(accuracy_score(y_test1, y_tree_guesses1)))
	print("Accuracy score for test_set_2: {}".format(accuracy_score(y_test2, y_tree_guesses2)))

	print("Generate our tree predictor..")
	print("Need a better visualization? Put it on http://webgraphviz.com/ :D")
	tree_file = open("tree.dot", 'w')
	export_graphviz(tree, out_file=tree_file, feature_names=data_training.columns.drop("Occupancy"),
					filled=True, rounded=True)
	tree_file.close()

	cm_peg_train = confusion_matrix(y_train, y_pegasos_train_guesses)
	cm_peg_test1 = confusion_matrix(y_test1, y_pegasos_guesses1)
	cm_peg_test2 = confusion_matrix(y_test2, y_pegasos_guesses2)

	cm_tree_train = confusion_matrix(y_train, y_tree_train_guesses)
	cm_tree_test1 = confusion_matrix(y_test1, y_tree_guesses1)
	cm_tree_test2 = confusion_matrix(y_test2, y_tree_guesses2)

	# Generate .png for every confusion matrix
	n = 0
	for confusion_matrix, title in [(cm_peg_train, " training set"), (cm_peg_test1, " test_set1"),
									(cm_peg_test2, "test_set2"), (cm_tree_train, " training set"),
									(cm_tree_test1, " test_set1"), (cm_tree_test2, " test_set2")]:

		title = "Matrice di confusione su " + title

		plt.figure()
		plot_confusion_matrix(confusion_matrix, classes=["Not Occupied", "Occupied"], title=title)
		plt.savefig('confusion_matrix_{}.png'.format(n))
		plt.close()
		n += 1
