from datetime import datetime
import io
import itertools
import zipfile
import matplotlib.pyplot as plt

from os.path import exists
from pandas import read_csv

import requests
import numpy as np


def is_a_week_day(dtime):
	"""Tests if the day given by date_time is a week day or not"""
	day_name = dtime.strftime("%A")

	return 0 if day_name == "Saturday" or day_name == "Sunday" else 1


def seconds_from_midnight(dtime):
	"""Returns number of seconds passed from midnight of the dtime parameter"""
	return dtime.hour * 3600 + dtime.minute * 60 + dtime.second


def extract_numbers_from_date(date_time):
	"""
	Exploits the 'date' attribute in the feature vector of Occupancy Detection Data Set, so that
	is usable for learning process of Pegasos algorithms, which works only on real numbers.
	:param date_time: a datetime parameter
	:return: a tuple containing results from is_a_week_day and seconds_from_midnight routines
	"""
	date_converted = datetime.strptime(date_time, "%Y-%m-%d %H:%M:%S")
	return seconds_from_midnight(date_converted), is_a_week_day(date_converted)


def load_dataset(data_path):
	"""
	Gets the data from path given as parameter. Since we have to transform 'date' column in two
	numers, collected by 'NMS' and 'WS' columns this function also transform the dataframe
	so that feature vector is a set of numbers. See paper linked in README for details.
	"""
	data = read_csv(data_path, header=0)
	data['NMS'] = data[['date']].apply(lambda x: extract_numbers_from_date(x['date'])[0], axis=1)
	data['WS'] = data[['date']].apply(lambda x: extract_numbers_from_date(x['date'])[1], axis=1)

	# Now we can drop 'date' column
	return data.drop(['date'], axis=1)


def check_dataset_presence():
	"""
	Checks the presence or not of Occupancy Detetection dataset files, and eventually fetches them from
	a specific url.
	"""

	if exists('dataset/datatraining.txt') and exists('dataset/datatest.txt') and exists('dataset/datatest2.txt'):
		return

	dataset_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/00357/occupancy_data.zip"

	print("Downloading dataset from ", dataset_url)
	r = requests.get(dataset_url, stream=True)
	z = zipfile.ZipFile(io.BytesIO(r.content))
	z.extractall('dataset')
	print("All files extracted in dataset/")


def plot_confusion_matrix(cm, classes, title='Matrice confusione ', cmap=plt.cm.Blues):
	"""
	This function prints and plots the confusion matrix.
	"""
	cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(len(classes))
	plt.xticks(tick_marks, classes, rotation=45)
	plt.yticks(tick_marks, classes)

	fmt = '.2f'
	thresh = cm.max() / 2.
	for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
		plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center",
				 color="white" if cm[i, j] > thresh else "black")

	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.tight_layout()
